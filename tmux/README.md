## pretty tmux configure
See README in .tmux

## tmuxinator
install tmuxinator in Ubuntu16.04
```shell
sudo apt-get purge --auto-remove ruby
add-apt-repository ppa:brightbox/ruby-ng
sudo apt-get update
sudo apt-get install ruby2.6 ruby2.6-dev
gem install tmuxinator
```
## .tmux
```
bash install_dot_tmux.sh
```
