https://www.tecmint.com/20-funny-commands-of-linux-or-linux-is-fun-in-terminal/
## Funny fonts
```shell
sudo apt-get update
sudo apt-get install sysvbanner
sudo apt-get install toilet
sudo apt-get install figlet
sudo apt-get install cowsay
sudo apt-get install cmatrix
sudo apt-get install bb
sudo apt-get install lolcat
sudo apt-get install boxes
```
example of usage:
### 1
` cowsay "hello,Shipley" `
 _______________
< hello,Shipley >
 ---------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

### 2
` toilet -f future --metal "Hello, Shipley" `
### 3
cool color and cute cat
`toilet -f ivrit 'Linux is fun!' | boxes -d cat -a hc -p h8 | lolcat`


## Animation in Terminal
Install ASCIIquarium
Now Download and Install ASCIIquarium.
```shell
# step 1
apt-get install libcurses-perl
cd /tmp 
wget http://search.cpan.org/CPAN/authors/id/K/KB/KBAUCOM/Term-Animation-2.4.tar.gz
tar -zxvf Term-Animation-2.4.tar.gz
cd Term-Animation-2.4/
perl Makefile.PL &&  make &&   make test
make install
# step 2
cd /tmp
wget http://www.robobunny.com/projects/asciiquarium/asciiquarium.tar.gz
tar -zxvf asciiquarium.tar.gz
cd asciiquarium_1.1/
cp asciiquarium /usr/local/bin
chmod 0755 /usr/local/bin/asciiquarium
# Now you can this command
asciiquarium
```
And finally run “asciiquarium” or “/usr/local/bin/asciiquarium“ in terminal without quotes and be a part of magic that will be taking place in front of your eyes.

