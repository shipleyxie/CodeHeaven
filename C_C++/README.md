## install gcc-7 in Ubuntu16.04
```
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install g++-7 -y
```
while using cmake, you need to configure enviroment through:
```
export CC=`which gcc-7`
export CXX=`which g++-7`
```
## gdbgui
```bash
# make sure gdb version bigger than 8.2
sudo apt-get install gdb
pip3 install pipx
pipx install gdbgui
pipx upgrade gdbgui
pipx uninstall gdbgui
```