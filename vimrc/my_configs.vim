
"highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE


"""""""""""""""""""
" => by shipley
"""""""""""""""""""
" color-scheme: gruvox
set cursorline
autocmd vimenter * ++nested colorscheme gruvbox

set relativenumber
set nu

" key mapping
" Group 1(F2-F4)
"noremap <F2> :YcmCompleter Format<CR>
nnoremap <F3> :UndotreeToggle<CR>
" Group 2(F5-F8)
"" F10 to toggle quickfix window
nnoremap <F5> :term<CR>
tnoremap <F6> <C-W>N
nnoremap <F7> :call asyncrun#quickfix_toggle(6)<cr>"

nnoremap [t :tabNext<cr>
nnoremap ]t :tabnext<cr>

" automatically open quickfix window when AsyncRun command is executed
" " set the quickfix window 6 lines height.
let g:asyncrun_open = 6
"" ring the bell to notify you job finished
let g:asyncrun_bell = 1
nnoremap <leader>r :AsyncRun -mode=term -pos=externel 

let g:indentLine_enabled = 0

let g:doxygen_enhanced_color=1
let g:DoxygenToolkit_briefTag_pre="@brief  "
let g:DoxygenToolkit_paramTag_pre="@param "
let g:DoxygenToolkit_returnTag="@return   "
let g:DoxygenToolkit_blockHeader="--------------------------------------------------------------------------"
let g:DoxygenToolkit_blockFooter="----------------------------------------------------------------------------"
let g:DoxygenToolkit_authorName="Shipley Xie"
let g:DoxygenToolkit_licenseTag="My own license"

"""""""""""""""""""
" => VIM
"""""""""""""""""""
" :!rm %    will delete current opened file.
" :set noignorecase
" g[t/T]   : will go to next/prev tab
"
" ctrl w f: will open file under cursor in a split window
" ctrl w gf : will open file under cursor in new tab
"   
" s in NerdTree will open file in split window
" o in NerdTree will open file
" i in NerdTree
" r in NerdTree will reload directory
"
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }

"   Folder
" zf17[j/k] : fold 17 lines the cursor line
" zd deletes the fold at the cursor.
" zE deletes all folds.
"
" jump to start/end of C++ function, use this: [{, ]}

"""""""""""""""""""
" => YouCompleteMe
"""""""""""""""""""
" IDE like:
" solution 1:
"   ctags -R *.c *.h
"   ctrl+w ]  : go to denfinition in a split window
" solution 2:
"   :YcmCompleter GoTo 
"   NOTE: there are some other subcommands in :YcmCompileter
nnoremap <leader>sd :YcmCompleter 
nnoremap <leader>] :YcmCompleter GoTo<CR>
nmap <leader>yfw <Plug>(YCMFindSymbolInWorkspace)
nmap <leader>yfd <Plug>(YCMFindSymbolInDocument)

" shows documentation in a popup at the cursor location after a short delay.
nmap gh <plug>(YCMHover)
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_auto_hover = ''

"  to use C/C++ syntax highlighting in the popup for C-family languages
augroup MyYCMCustom
  autocmd!
  autocmd FileType c,cpp let b:ycm_hover = {
      \ 'command': 'GetDoc',
      \ 'syntax': &filetype
      \ }
augroup END

" Let clangd fully control code completion
let g:ycm_clangd_uses_ycmd_caching = 0
" Use installed clangd, not YCM-bundled clangd which doesn't get updates.
let g:ycm_clangd_binary_path = exepath("clangd")
let g:ycm_clangd_args = [ '--header-insertion=never' ]

" FOR C/C++
let g:ycm_extra_conf_globlist=['~/.vim_runtime/ycm_extra_conf_C.py', '/media/shipley/Disk21/Project/webrtc/rtcAsp/.ycm_extra_conf.py'] 
let g:ycm_global_ycm_extra_conf = '~/.vim_runtime/ycm_extra_conf_C.py'

" FOR Python
let g:ycm_python_interpreter_path = '~/anaconda3/envs/audio/bin/python'
let g:ycm_python_sys_path = ['~/anaconda3/envs/audio/lib/python3.7/site-packages']
let g:ycm_extra_conf_vim_data = [
  \  'g:ycm_python_interpreter_path',
  \  'g:ycm_python_sys_path'
  \]
let g:ycm_global_ycm_extra_conf = '~/.vim_runtime/ycm_extra_conf_python.py'


"""""""""""""""""""
" => vim-autoformat
"""""""""""""""""""
noremap <F2> :Autoformat<CR>
let g:formatdef_my_custom_c = '"clang-format"'
let g:formatters_c = ['my_custom_c']

" let g:formatdef_my_custom_python = '"clang-format -style=google"'
let g:formatters_python = ['yapf']
let g:formatter_yapf_style = 'pep8'

" auto format the diff of Git, use this cmd:
" git diff -U0 --no-color HEAD^ | clang-format-diff -i -p1

"""""""""""""""""""
" => vim-header
"""""""""""""""""""
let g:header_auto_add_header = 0
let g:header_field_author = 'Shipley Xie'
let g:header_field_author_email = 'shipleyxie@outlook.com'

"""""""""""""""""""
" => Airline plugin
"""""""""""""""""""
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline_theme='badwolf'
" :AirlineToggle    and then you can jump with buffer number
" :b24   jump to buffer with number 24

"""""""""""""""""""
" => vim-fugitive
"""""""""""""""""""
" ds/dv split or vertical split git diffrence
" :Ggrep -EIi "todo|fixme"
nnoremap gs :Gstatus<cr>

"""""""""""""""""""
" => vim-bookmark
"""""""""""""""""""
"| Action                                          | Shortcut    | Command                      |
"|-------------------------------------------------|-------------|------------------------------|
"| Add/remove bookmark at current line             | `mm`        | `:BookmarkToggle`            |
"| Add/edit/remove annotation at current line      | `mi`        | `:BookmarkAnnotate <TEXT>`   |
"| Jump to next bookmark in buffer                 | `mn`        | `:BookmarkNext`              |
"| Jump to previous bookmark in buffer             | `mp`        | `:BookmarkPrev`              |
"| Show all bookmarks (toggle)                     | `ma`        | `:BookmarkShowAll`           |
"| Clear bookmarks in current buffer only          | `mc`        | `:BookmarkClear`             |
"| Clear bookmarks in all buffers                  | `mx`        | `:BookmarkClearAll`          |
"| Move up bookmark at current line                | `[count]mkk`| `:BookmarkMoveUp [<COUNT>]`  |
"| Move down bookmark at current line              | `[count]mjj`| `:BookmarkMoveDown [<COUNT>]`|
"| Move bookmark at current line to another line   | `[count]mg` | `:BookmarkMoveToLine <LINE>` |
"| Save all bookmarks to a file                    |             | `:BookmarkSave <FILE_PATH>`  |
"| Load bookmarks from a file                      |             | `:BookmarkLoad <FILE_PATH>`  |
