
## nerd tree font for vim-devicons
Set font "BlexMono Nerd Font Medium" in terminal, but in WSL and VScode set to "BlexMono NF" instead.
`cp nerd-fonts/IBMPlexMono/Mono/complete/*.ttf ~/.local/share/fonts`
or if you want to install font for all user:
`cp nerd-fonts/IBMPlexMono/Mono/complete/*.ttf /usr/local/share/fonts/`

