<!-- TOC -->

- [1. ZSH](#1-zsh)
	- [1.1. VSCode](#11-vscode)
	- [1.2. Plugins for VSCode](#12-plugins-for-vscode)
- [2. VIM](#2-vim)
	- [2.1. REQUIREMENTS](#21-requirements)
	- [2.2. termdebug](#22-termdebug)
	- [2.3. INSTALL PULUGIN](#23-install-pulugin)
	- [2.4. KEY MAPPINGS](#24-key-mappings)
	- [2.5. PLUGIN](#25-plugin)
- [3. TMUX](#3-tmux)
	- [3.1. tmux](#31-tmux)
- [4. GIT](#4-git)
	- [4.1. commitizen](#41-commitizen)
- [5. Doxygen](#5-doxygen)

<!-- /TOC -->

My coding tools and configuration.
[![asciicast](https://asciinema.org/a/XpPj6akLokqLwj6rSGCGAGioB.svg)](https://asciinema.org/a/XpPj6akLokqLwj6rSGCGAGioB)

# 1. ZSH

change color schemes `Right clik on terminal, enter profile Preference, choose Colors -> Solarized dard`
```bash
sudo apt-get install zsh
sudo apt-get install powerline  #install necessary font
# use ohmyzsh
unzip ohmyzsh-master.zip
cd ohyzshg-master/tools && sh isntall.sh
# use agnoster.zsh-theme
cp zsh/agnoster.zsh-theme ~/.oh-my-zsh/themes/agnoster.zsh-theme
# if you using Conda, following config is need
conda config --set auto_activate_base false
conda config --set auto_activate_base true
conda config --set changeps1 False	# avoid duplicate name of env
# install zsh syntax highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

## 1.1. VSCode

enter preferrence->setting->terminal fonts, set` 'Ubuntu Mono', 'PowerlineSymbols'`

## 1.2. Plugins for VSCode
Bash Debug
Bracker Pair Colorizer

C/C++
C++ Intellisense
Native Debug
clangd

Python

Shell-format
shellman

VIM

Cmake
Cmake Tools
cmake-format

Git Graph
Git history
GitLens

Perl (publisher:"Gerald Richter", run:  `sudo cpan Perl::LanguageServer`, `sudo cpan install Moose`,`sudo cpan PadWalker`,`sudo apt-get install libpadwalker-perl`)

# 2. VIM

## 2.1. REQUIREMENTS

```bash
git clone https://gitee.com/shipleyxie/CodeHeaven.git
cd CodeHeaven
# compile and install vim8.2

# if you  have installed vim before
sudo apt-get remove \
		vim \
		vim-runtime \
		gvim \
		vim-tiny \
		vim-common \
		vim-gui-common
# install needed lib
sudo apt-get install \
		libncurses5-dev \
		libgnome2-dev \
		libgnomeui-dev \
		libgtk2.0-dev \
		libatk1.0-dev \
		libbonoboui2-dev \
		libcairo2-dev \
		libx11-dev \
		libxpm-dev \
		libxt-dev \
		python-dev python3-dev ruby-dev \
		mercurial

# use latest git
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install -f
sudo apt upgrade

# compile Python3.6 to use YouCompleteMe
wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz
./configure --enable-shared
make 
make install
sudo cp /usr/local/lib/libpython3.6m.so.1.0 /usr/lib

# install needed lib for YouCompleteMe
sudo apt-get install mono-complete
sudo apt-get install npm

# source compile VIM
unzip vim8.2-source.zip
cd vim-master
# configure with huge feature and enable python
./configure --with-features=huge \
		--enable-multibyte \
		--enable-rubyinterp \
		--enable-python3interp=yes \
		--with-python3-config-dir=$(python3.6-config --configdir) \
		--enable-perlinterp \
		--enable-luainterp \
		--enable-gui=gtk2 \
		--enable-cscope \
		--prefix=/usr/local
# make sure you are at vim-master, then run
make -j 8 && sudo make install

# plugins used for auto format in VIM
sudo apt-get install 
    python-autopep8 \
    astyle \
    clang-format \
    ack-grep
pip install yapf
pip install --user black
pip install yapf
sudo apt-get install python-pip
# github.com/wklken/k-vim   <-  C_vim plugin ref

cp -rf vimrc ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

# install YouCompleteMe
cd my_plugins/youcompletme
git submodule update --init --recursive

python3.6 install.py --clangd-completer		# enable C++
python3.6 install.py --all			# enable all

# when it come to "tarfile.ReadError: file could not be opened successfully"
# change to python2 and do tarfile.extractall manually

# NOTE 1: 
pip install compiledb	# used for make
# compiledb make 
# NOTE 2: 
# If using CMake, add -DCMAKE_EXPORT_COMPILE_COMMANDS=ON 
# when configuring (or add set( CMAKE_EXPORT_COMPILE_COMMANDS ON ) to CMakeLists.txt) 
# and copy or symlink the generated database to the root of your project.

# color_coded
sudo apt-get install build-essential libclang-3.9-dev libncurses-dev libz-dev cmake xz-utils libpthread-workqueue-dev
cd color_coded
mkdir build && cd build && cmake ..
make && make install
```

`sudo apt install jq`  # Command-line JSON processor
then you can format json file by using `:%!jq .` in vim.

## 2.2. termdebug

Use this command `:packdd termdebug` to load termdebug which is very useful when debugging C.

## 2.3. INSTALL PULUGIN

```bash
cd ~/.vim_runtime
git clone git://github.com/tpope/vim-rails.git my_plugins/vim-rails
```

## 2.4. KEY MAPPINGS

The \<leader\> represent [ , ](https://gitee.com/shipleyxie/bashrc_vimrc/blob/master/vimrc/vimrcs/basic.vim#L46)

## 2.5. PLUGIN

Clone plugin file into [~/.vim_runtime/my_plugins](https://gitee.com/shipleyxie/bashrc_vimrc/blob/master/vimrc/vimrcs/plugins_config.vim#L9), reopen VIM it will be installed automatically by [pathogen](https://gitee.com/shipleyxie/bashrc_vimrc/blob/master/vimrc/vimrcs/plugins_config.vim#L14)

```bash

cd ~/.vim_runtime
git clone git://github.com/tpope/vim-rails.git my_plugins/vim-rails/
git clone git://github.com/majutsushi/tagbar my_plugins/tagbar/
```

If you want to upgrade plugins, please following the instruction as below

```bash
# update plugins
cd ~/.vim_runtime
#git pull --rebase
python update_plugins.py
```

my .vimrc with mapping of vim-header

```bash
 set runtimepath+=~/.vim_runtime
 
 source ~/.vim_runtime/vimrcs/basic.vim
 source ~/.vim_runtime/vimrcs/filetypes.vim
 source ~/.vim_runtime/vimrcs/plugins_config.vim
 source ~/.vim_runtime/vimrcs/extended.vim
 
 try
 source ~/.vim_runtime/my_configs.vim
 catch
 endtry
 
 let g:header_field_author = 'Your Name'
 let g:header_field_author_email = 'your@mail'
 map <F4> :AddHeader<CR>
```


# 3. TMUX
## 3.1. tmux
ctrl B ctrl f	: find another session
ctrl B f 	: find another window

# 4. GIT

## 4.1. commitizen

Automatically generate commit messages.

```bash
sudo npm install -g npm
sudo npm install -g commitizen
npm install commitizen -g

# initialize your project use:
commitizen init cz-conventional-changelog --save-dev --save-exact
# then you can use
git cz
```

# 5. Doxygen
```bash

sudo apt-get install flex
sudo apt-get install bison
sudo apt-get install graphviz
cd doxygen
mkdir build && cd build
cmake -G "Unix Makefiles" ..
make
sudo make install
```

# 6. xournal

Xournal is a notetaking software

```
sudo apt-get install xournal
```

# Upgrade gcc/g++

```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt install gcc-10
For g++-10:

sudo apt install g++-10
```
# terminal

下载`dconf-editor`修改其默认值
```bash
sudo apt-get install dconf-editor
```