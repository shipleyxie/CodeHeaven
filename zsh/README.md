# Plugins in zsh
## syntax-highlighting, zsh-autosuggestions
```shell
#syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
#zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
vim ~/.zshrc
```
and change like this,
 plugins=(
     git
     zsh-syntax-highlighting
     zsh-autosuggestions
     )
than `source ~/.zshrc` to activate zsh-syntax-highlighting and zsh-autosuggestion
