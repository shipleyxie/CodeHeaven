local opts = {
  cmd = {
    "clangd",
    "--query-driver=aarch64-none-linux-gnu-*",
  },
   filetypes = { "c", "cpp", "objc", "objcpp", "cuda", "proto" },
}

return opts
