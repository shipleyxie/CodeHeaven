-- https://github.com/nvim-tree/nvim-tree.lua/wiki/Migrating-To-on_attach
local function on_attach(bufnr)
  local api = require("nvim-tree.api")

  local function opts(desc)
    return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end

  -- default mappings
  api.config.mappings.default_on_attach(bufnr)
  -- <C-v> open vertivally
  -- <C-x> open horizontal
  -- <C-]> change root to node
  -- <C-k> Info
  -- . run command for current node

  -- custom mappings
  vim.keymap.set("n", "h", api.node.navigate.parent_close, opts("Close Directory"))

  vim.keymap.set("n", "yn", api.fs.copy.filename, opts("Copy Name"))
  vim.keymap.set("n", "yr", api.fs.copy.relative_path, opts("Copy Relative Path"))
  vim.keymap.set("n", "ya", api.fs.copy.absolute_path, opts("Copy Absolute Path"))
  vim.keymap.set("n", "a", api.fs.create, opts("Create"))
  vim.keymap.set("n", "d", api.fs.remove, opts("Delete"))
  vim.keymap.set("n", "r", api.fs.rename, opts("Rename"))

  vim.keymap.set("n", "<C-r>", api.tree.reload, opts("Refresh"))
  vim.keymap.set("n", "I", api.tree.toggle_gitignore_filter, opts("Toggle Git Ignore"))
  vim.keymap.set("n", "R", api.tree.collapse_all, opts("Collapse"))
  vim.keymap.set("n", "?", api.tree.toggle_help, opts("Help"))
end

require("nvim-tree").setup({
  view = {
    side = "right",
  },
  on_attach = on_attach,
  actions = {
    open_file = {
      quit_on_open = false,
      window_picker = {
        enable = false,
      },
    },
  },
})
