if [ -f ~/.tmux.conf ]; then rm ~/.tmux.conf; fi
if [ -f ~/.tmux.conf.local ]; then rm ~/.tmux.conf.local; fi


PWD=`pwd`
ln -s -f $PWD/.tmux/.tmux.conf ~/.tmux.conf
cp $PWD/.tmux/.tmux.conf.local ~/.tmux.conf.local
